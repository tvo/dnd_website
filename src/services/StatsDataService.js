export class StatsDataService
{
	stats = ['ac', 'con', 'str', 'dex', 'wis', 'int', 'cha'];
	group_by_key = 'cr';
  monsters = [];

	setData(data) {
    monsters = data;
	}

	transformData_xyStat(api_response) {
		let refdata = [];
		if(api_response === undefined || api_response.length === 0)
			return [];
		for(let stat_name of this.stats) {
			for(let api_groups of api_response) {
				let x = parseFloat(api_groups[this.group_by_key]);
				if(x === undefined)
					continue;
				let s_group = api_groups[stat_name];
				if(s_group === undefined)
					continue;
				for(let bin_group of s_group) {
					refdata.push(...Array(bin_group.c).fill(bin_group.v).map(v => {
						let r = {x: x};
						r[stat_name] = v;
						return r;
					}));
				}
			}
		}
		return refdata;
	}
}

export function quartiles_from_array(data) {
	data.sort(function(a, b){return a - b});
	let len = data.length;
	let q1 = Math.floor((len / 4));
	let q2 = Math.floor((len / 2));
	let q3 = Math.floor((len * 3 / 4));
	return {
		q1: data[q1],
		q2: data[q2],
		q3: data[q3],
	}
}

export function average(array) { return array.reduce((a, b) => a + b) / array.length; }
