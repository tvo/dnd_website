export class CharacterBuildService {
}

export function fillCalculatedFnForHeader(header) {
	if(!header || !header.default || header.default === '')
		return header;
	try
	{
		header.default_fn = calculate(header.default);
		//console.log(header.default_fn);
	}
	catch(e){
		console.log(header.default);
		throw e;
	}
	return header;
}

export function fillCharacterLevelStats(state, stats, max_level) {
	state = JSON.parse(JSON.stringify(state));
	for(var level = 1; level <= max_level; ++level) {
		if(state[level] === undefined)
			state[level] = {};
		let character = state[level];
		character.level = level;
		let flatData = {level};
		for(var stat of stats) {
			if(character[stat.name]?.user_set === true) {}
			else if(level === 1 || stat.fill_lower !== true) {
				if(!!stat.default && typeof stat.default_fn === 'function')
					character[stat.name] = { value: stat.default_fn(flatData) };
				else
					character[stat.name] = { value: 0 };
			}
			else
				character[stat.name] = { value: state[level - 1][stat.name].value };
			flatData[stat.name] = character[stat.name].value;
		}
	}
	return state;
}
