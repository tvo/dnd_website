export function InputElement({ type, label, value, onChange}) {
	return (
		<div className='input'>
		<label hidden={label === ''}>{label}</label><input type={type} onChange={e => onChange(e.target.value)} value={value} />
		</div>
	);
}

